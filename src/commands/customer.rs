use serenity::{
    utils::Colour,
    framework::standard::{
        CommandResult,
        macros::{command, group},
    },
	model::channel::Message,
    prelude::Context,
};

#[group]
#[commands(order,ping)]
struct Customer;

#[command]
fn ping(context: &mut Context, msg: &Message) -> CommandResult{
    let dm = msg.channel_id.say(&context.http, "Pong!");

    if let Err(why) = dm {
        println!("Error when direct messaging user: {:?}", why);
    }

    Ok(())
}

#[command]
fn order(context: &mut Context, msg: &Message) -> CommandResult{
    let dm = msg.author.dm(&context, |m| {
        m.embed(|e| {
            e.color(Colour::from_rgb(7, 68, 141));
            e.title("This is a title");
            e.description("This is a description");
            e.fields(vec![
                ("This is the first field", "This is a field body", true),
                ("This is the second field", "Both of these fields are inline", true),
            ]);
            e.field("This is the third field", "This is not an inline field", false);
            e.footer(|f| {
                f.text("This is a footer");

                f
            });

            e
        });

        m
    });

    if let Err(why) = dm {
        println!("Error when direct messaging user: {:?}", why);
    }

    Ok(())
}


mod commands;
use std::env;
use serenity::{
    model::{
        gateway::Ready,
    },
    prelude::*,
};
use serenity::framework::standard::StandardFramework;
use commands::{
    CUSTOMER_GROUP,
};

struct Handler;

impl EventHandler for Handler{
    
    fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

fn main() {
    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment");
    let mut client = Client::new(&token, Handler).expect("Err creating client");
    
    client.with_framework(
		StandardFramework::new()
            .configure(|c| c.prefix("$"))
            .group(&CUSTOMER_GROUP),
	);

    if let Err(why) = client.start() {
        println!("Client error: {:?}", why);
    }   
}
